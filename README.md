# Ruche Statistics

A python script to plot the beamline statistics at SOLEIL using CSV files.


## Usage

General case:
```
python3 plot_stats.py ruche-*.csv
```

You can restrict the plot to some beamlines and change the units to TB (instead of GB) using the CSV files you want:
```
python3 plot_stats.py ruche-2020.csv -b ANATOMIX CRISTAL NANOSCOPIUM PROXIMA1 PROXIMA2a PSICHE SAMBA SWING -u TB
```

Beammlines can be sorted (based on the accumulated volumes):
```
python3 plot_stats.py ruche-*.csv -s
```

