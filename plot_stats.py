#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import matplotlib.pyplot as plt
import numpy as np
import re

from read_csv import read_csv_to_list

DEFAULT_VOLUME_UNITS = 'GB'
VOLUME_UNIT_CHOICES = ['GB', 'TB', 'PB', 'EB']

GB_TO_TB = 1e-3
GB_TO_PB = 1e-6
GB_TO_EB = 1e-9

class UnknownBeamline(Exception):
    pass


class ScopeNotFound(Exception):
    pass


def get_scope_from_file(csv_file):
    """"
    Return the scope (e.g, year, month, etc.) using the name of the file
    For example, a file named 'ruche-2022-03-23.csv' has the scope '2022-03-23'
    """
    found = re.search("ruche-(.*).csv", csv_file)
    if found:
        return found.group(1)
    else:
        raise ScopeNotFound


def get_volumes(csv_files, units=DEFAULT_VOLUME_UNITS):
    """
    Return the volume of produced data in GB for each beamline. The output results will look like this:
    'ANATOMIX': {'2020': 193.745852, '2021': 280.909}, 'CRISTAL': {'2020': 3.535275, '2021': 4.696}, ...
    Volumes in TB are obtained using units='TB'
    """
    results = {}
    for index, csv_file in enumerate(csv_files):
        try:
            scope = get_scope_from_file(csv_file)
        except ScopeNotFound:
            scope = index
        data = read_csv_to_list(csv_file, labels=['beamline', 'nb_files', 'total', 'mean'], separator=';', comment_sign='#')
        for d in data:
            beamline = d['beamline']
            volumes = d['total']
            if units == 'TB': volumes = volumes * GB_TO_TB
            if units == 'PB': volumes = volumes * GB_TO_PB
            if units == 'EB': volumes = volumes * GB_TO_EB
            results.setdefault(beamline.upper(), {}).update({scope: volumes})
    return dict(sorted(results.items()))


def check_wanted_beamlines(wanted_beamlines, beamlines):
    """Check that the beamlines specified in the input are referenced"""
    unknown_beamlines = []
    for b in wanted_beamlines:
        if b not in beamlines:
            unknown_beamlines.append(b)
    if len(unknown_beamlines) > 0:
        raise UnknownBeamline(unknown_beamlines)


def add_labels(rects):
    """
    Function to add value labels on a bar plot
    Will be deprectaed starting from version 1.5 of matplotlib...
    """
    for rect in rects:
        height = rect.get_height()
        if height < 1:
            plt.text(rect.get_x() + rect.get_width() / 2.0, height, f'{height:.0e}', ha='center', va='bottom')
        else:
            plt.text(rect.get_x() + rect.get_width() / 2.0, height, f'{height:.0f}', ha='center', va='bottom')

        
def make_histograms(csv_files, wanted_beamlines=None, volume_units=DEFAULT_VOLUME_UNITS, sort_by_volume=False):
    """
    Create an histogram with bar side-by-side
    if sort_by_volume is set to True, beamlines are sorted by the sum of all scope (e.g., yearly-, monthly-) volumes
    Example:
    make_histograms(csv_files, wanted_beamlines=['ANATOMIX', 'PSICHE'], volume_units='TB', sort_by_volume=True)
    """
    volumes = get_volumes(csv_files, units=volume_units)
    beamlines = volumes.keys()

    if wanted_beamlines:
        check_wanted_beamlines(wanted_beamlines, beamlines)
        volumes = {k: v for k, v in volumes.items() if k in wanted_beamlines}
    
    if sort_by_volume:
        volumes = dict(sorted([(k,v) for k,v in volumes.items()], key=lambda x: sum(x[1].values()), reverse=True))

    beamlines = volumes.keys()  # to respect the sorting
    nb_beamlines = len(beamlines)

    for index, csv_file in enumerate(csv_files):
        try:
            scope = get_scope_from_file(csv_file)
        except ScopeNotFound:
            scope = index
        volumes_per_scope = []  # volumes for a given year or month, or anything else
        for beamline_volume_per_scope in volumes.values():
            try:
                v = beamline_volume_per_scope[scope]
            except KeyError:
                v = 0
            volumes_per_scope.append(v)

        # Total volume (per year or month, or anything else)
        print("Scope: {}, Total volume = {:.2f}".format(scope, sum(volumes_per_scope)))
        
        # Plot
        x_axis = np.arange(nb_beamlines)
        width = 0.25
        rects = plt.bar(x_axis + index * width, volumes_per_scope, width, label=scope)
        add_labels(rects)

    plt.yscale('log')
    plt.xticks(x_axis, beamlines, rotation=45)
    plt.ylabel("Volume of data produced ({})".format(volume_units))

    plt.legend(loc='upper right')
    plt.show()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('csv_files', type=str, nargs='+', help="CSV files containing the statistics")
    parser.add_argument('-b','--beamlines', type=str.upper, nargs='+', default=None)
    parser.add_argument('-u','--volume_units', type=str.upper, choices=VOLUME_UNIT_CHOICES, default=DEFAULT_VOLUME_UNITS)
    parser.add_argument('-s', '--sort-by-volume', action=argparse.BooleanOptionalAction)
    args = parser.parse_args()

    make_histograms(args.csv_files, wanted_beamlines=args.beamlines, volume_units=args.volume_units, sort_by_volume=args.sort_by_volume)

