#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re


def read_csv_to_list(filename, labels=None, comment_sign=';', separator=','):
    """Return the parameters from a csv data file. The output is a list of dicts"""
    results = []
    with open(filename, 'r') as f:
        for row in f:
            row = row.rstrip('\n')
            if row and not row.startswith(comment_sign):
                column_items = re.split(separator, row)
                if not labels:
                    labels = ['col_{}'.format(str(x)) for x in range(len(column_items))]
                element = {}
                for j, label in enumerate(labels):
                    item = column_items[j]
                    try:
                        value = float(item)
                    except ValueError:
                        value = item
                    element[label] = value
                results.append(element)
    return results


def read_csv_to_dict(filename, labels=None, comment_sign=';', separator=',', reverse=False):
    """Return the parameters from a csv data file. The output is a dict where each key has list values"""
    result = {}
    with open(filename, 'r') as f:
        for row in f:
            row = row.rstrip('\n')
            if row and not row.startswith(comment_sign):
                column_items = re.split(separator, row)
                if not labels:
                    labels = ['col_{}'.format(str(x)) for x in range(len(column_items))]
                for j, label in enumerate(labels):
                    item = column_items[j]
                    try:
                        value = float(item)
                    except ValueError:
                        value = item
                    if reverse:
                        result.setdefault(label, []).insert(0, value)  # append in reverse order
                    else:
                        result.setdefault(label, []).append(value)
    return result
